# Written by Tom Kurowski <t.j.kurowski@cranfield.ac.uk>
"""Tools for mapping variant coordinates between genomes.

This module provides a function which compares two sets of sequences and
creates a "mapping" between them, listing the respective positions of
contigs/scaffolds in both sets.

When comparing two versions of a reference genome, this mapping
represents a list of differences in scaffold position and orientation
between them.

The module also provides functions which can use a "scaffold mapping" to
update the coordinates and sequences in variant call format (VCF) files.

No extra information (e.g. AGP files) about the actual contig/scaffold
organisation of the genomes is required, as this is inferred from the
sequences.

"Contigs" are defined as stretches of sequence containing no more than
one N base in a row. "Scaffolds" are defined as groups of adjacent
contigs which have the same relative order, position, and orientation
in both sets of sequences being compared.

Functions:

    create_sequence_mapping(file_from, file_to, index, group)
        Create sequence position mapping between two FASTA files.

    update_vcfs(output_dir, scaffold_map, *input_files)
        Update multiple VCF files based on a sequence mapping.

    #update_vcf(input_file, output_file, scaffold_map):
     #   Update a single VCF file based on a sequence mapping.
"""
import collections
import csv
import datetime
import functools
import hashlib
import itertools
import multiprocessing
import os
import re
#import ssdeep
import sys

from timeit import default_timer as timer

from Bio import SeqIO # need to install biopython
from Bio.Seq import Seq # need to install biopython

FeaturePosition = collections.namedtuple('FeaturePosition',
                                         ('seq_id', 'start', 'end'))

FeatureMapping = collections.namedtuple('FeatureMapping',
                                        ('old_pos', 'new_pos', 'reversed'))

def _extract_contig_positions(sequence_dict, nproc=None):
    pool = multiprocessing.Pool(nproc)
    results = pool.map(_sequence_extract_contig_positions, sequence_dict.values())
    pool.close()
    pool.join()
    # Flatten result list
    results = list(itertools.chain.from_iterable(results))
    # Sort by sequence id and start position
    results.sort(key=lambda x: (x.seq_id, x.start))
    return results

def _sequence_extract_contig_positions(sequence_record):
    # Extract a list of contig positions from FASTA sequence.
    sequence = str(sequence_record.upper().seq)
    # Gaps are easier to find than contigs so we find those
    # and get contigs around them.
    gaps = [gap for gap in re.finditer('N{2,}', sequence)]
    positions = []
    # No gaps, the entire sequence is a single contig
    if not len(gaps):
        positions.append(FeaturePosition(seq_id=sequence_record.id,
                                         start=1, end=len(sequence)))
        return positions
    # Unless sequence starts with a gap, add first contig
    if gaps[0].start() != 0:
        positions.append(FeaturePosition(seq_id=sequence_record.id,
                                         start=1,
                                         end=gaps[0].start()))
    for i in range(1, len(gaps)):
        positions.append(FeaturePosition(seq_id=sequence_record.id,
                                         start=gaps[i-1].end()+1,
                                         end=gaps[i].start()))
    # Unless sequence ends with a gap, add final contig
    if gaps[len(gaps)-1].end() != (len(sequence)-1):
        positions.append(FeaturePosition(seq_id=sequence_record.id,
                                         start=gaps[len(gaps)-1].end()+1,
                                         end=len(sequence)))
    return positions

def _get_contig_sequence(sequence, contig_position):
    return sequence[(contig_position.start - 1):contig_position.end]

def _get_sequence_hash(sequence, reverse_complement=False):
    # Return MD5 hash of a forward/reverse complement base sequence.
    if reverse_complement:
        sequence = str(Seq(sequence).reverse_complement())
    if sys.version_info[0]>2:
        return hashlib.md5(sequence.encode('utf-8')).hexdigest() # For Python3
    else:
        return hashlib.md5(sequence).hexdigest() # faster

def _get_contig_hashes(contig_positions, sequence_dict, reverse_complement=False):
    # Create dictionary of forward/reverse contig sequence hashes (keys)
    # associated with contig positions (values).
    contig_hashes = {}
    current_seq_id = ''
    for contig_pos in contig_positions:
        if (contig_pos.seq_id != current_seq_id):
            # Here "sequence" is the entire sequence of a FASTA entry
            sequence = str(sequence_dict[contig_pos.seq_id].seq).upper()
            current_seq_id = contig_pos.seq_id
        contig_seq = _get_contig_sequence(sequence, contig_pos)
        hash_key = _get_sequence_hash(contig_seq, reverse_complement)
        # TODO: implement hash collision detection and handling
        contig_hashes[hash_key] = contig_pos
    return contig_hashes

def _get_fasta_sequences(filename, index=False):
    if index:
        # Slower, uses less memory
        return SeqIO.index(filename, 'fasta')
    else:
        # Faster, uses more memory
        with open(filename, 'rU') as handle:
            return SeqIO.to_dict(SeqIO.parse(handle, 'fasta'))

def _relative_position_preserved(mapping_a, mapping_b):
    # Compare mappings of two contigs to check if their relative
    # position and orientation were preserved.
    if (mapping_a.old_pos.seq_id != mapping_b.old_pos.seq_id):
        return False
    if (mapping_a.reversed != mapping_b.reversed):
        return False
    # Check if size of gaps changed
    old_size = mapping_a.old_pos.end - mapping_b.old_pos.start
    if (mapping_a.reversed):
        new_size = mapping_b.new_pos.end - mapping_a.new_pos.start
    else:
        new_size = mapping_a.new_pos.end - mapping_b.new_pos.start
    return old_size == new_size

def _span_mappings(from_map, to_map):
    # Creates a feature mapping which starts at the start of from_map
    # and ends at the end of to_map
    old_scaffold = FeaturePosition(from_map.old_pos.seq_id,
                                    from_map.old_pos.start,
                                    to_map.old_pos.end)
    if (from_map.reversed):
        new_scaffold = FeaturePosition(from_map.new_pos.seq_id,
                                to_map.new_pos.start,
                                from_map.new_pos.end)
    else:
        new_scaffold = FeaturePosition(from_map.new_pos.seq_id,
                                        from_map.new_pos.start,
                                        to_map.new_pos.end)
    return FeatureMapping(old_scaffold, new_scaffold, from_map.reversed)

def _map_features_by_hash(hashes_from, hashes_to,
                          features_from, features_to, reversed=False):
    contig_map = []
    shared_hashes = set(hashes_from.keys()).intersection(hashes_to.keys())
    for key in shared_hashes:
            contig_map.append(FeatureMapping(hashes_from[key],
                                             hashes_to[key], reversed))
            features_from.remove(hashes_from[key])
            features_to.remove(hashes_to[key])
            del hashes_from[key]
    return contig_map

def _group_mappings(feature_map):
    if len(feature_map) <= 1:
        return feature_map
    # Group contig mappings into scaffold mappings
    grouped_map = []
    group_first_feature = None
    group_last_feature = None

    for feature in feature_map:
        # Start first scaffold
        if group_first_feature is None:
            group_first_feature = feature
            group_last_feature = feature
            continue
        # Extend scaffold if relative position and orientation preserved
        if (_relative_position_preserved(feature, group_last_feature)):
            group_last_feature = feature
        else:
            # Append previous scaffold
            grouped_map.append(_span_mappings(group_first_feature,
                                              group_last_feature))
            # Start new scaffold
            group_first_feature = feature
            group_last_feature = feature
    # Append final scaffold
    grouped_map.append(_span_mappings(group_first_feature,
                                      group_last_feature))
    return grouped_map

def _create_sequence_mapping(sequences_from, sequences_to, nproc=1, group=True):
    contigs_from = _extract_contig_positions(sequences_from, nproc)
    contigs_to = _extract_contig_positions(sequences_to, nproc)

    # Match forward hashes
    forward_hashes_from = _get_contig_hashes(contigs_from, sequences_from)
    forward_hashes_to = _get_contig_hashes(contigs_to, sequences_to)
    contig_map = _map_features_by_hash(forward_hashes_from,
                                       forward_hashes_to,
                                       contigs_from, contigs_to)
    # Match reverse hashes
    if contigs_from and contigs_to:
        reverse_hashes_to = _get_contig_hashes(contigs_to, sequences_to,
                                               reverse_complement=True)
        contig_map += _map_features_by_hash(forward_hashes_from,
                                            reverse_hashes_to,
                                            contigs_from, contigs_to,
                                            reversed=True)

    # Sort first by (old) sequence id, then by (old) position
    contig_map.sort(key=lambda x: (x.old_pos.seq_id,
                                   x.old_pos.start))

    # TODO: return deleted/added contigs as well
    if group:
        contig_map = _group_mappings(contig_map)
    return (contig_map, contigs_from, contigs_to)

def create_sequence_mapping(file_from, file_to, nproc=1, index=False, group=True):
    """ Create sequence position mapping between two fasta files.

    Create a scaffold (if group is True) or contig (if group is False)
    mapping between the sequences in file_from and the ones in file_to.
    The index parameter can be set to True to limit memory use, but
    it lowers the speed of execution.

    Usage:
    >>>from seqremap import create_sequence_mapping
    >>>mapping = create_sequence_mapping("SL2.40.fa", "SL2.50.fa")
    """
    sequences_from = _get_fasta_sequences(file_from, index)
    sequences_to = _get_fasta_sequences(file_to, index)
    return _create_sequence_mapping(sequences_from, sequences_to, nproc, group)

def _remap_snp(variant, mapping):
    pos_in_scaffold = int(variant[1]) - mapping.old_pos.start
    variant[0] = mapping.new_pos.seq_id
    if (mapping.reversed):
        variant[1] = str(mapping.new_pos.end - pos_in_scaffold)
        variant[3] = str(Seq(variant[3]).complement())
        variant[4] = str(Seq(variant[4]).complement())
    else:
        variant[1] = str(mapping.new_pos.start + pos_in_scaffold)

def _is_snp(variant):
    # If REF allele and all ALT alleles are all single nucletides
    if len(variant[3]) == 1 and all([len(alt)==1 for alt in variant[4].split(',')]):
        return True
    return False

def _reorient_indel_seq(sequence, preceding_base):
    return preceding_base + str(Seq(sequence[1:]).reverse_complement())

def _remap_indel(variant, mapping, sequence_dict):
    pos_in_scaffold = int(variant[1]) - mapping.old_pos.start
    variant[0] = mapping.new_pos.seq_id
    if (mapping.reversed):
        new_position = mapping.new_pos.end - pos_in_scaffold - len(variant[3])
        variant[1] = str(new_position)
        preceding_base = str(sequence_dict[variant[0]].seq[new_position-1]).upper()
        variant[3] = _reorient_indel_seq(variant[3], preceding_base)
        new_alt_alleles = [_reorient_indel_seq(alt, preceding_base) for alt in variant[4].split(',')]
        variant[4] = ','.join(new_alt_alleles)
    else:
        variant[1] = str(mapping.new_pos.start + pos_in_scaffold)

def _remap_variant(variant, scaffold_map_dict, sequence_dict):
    pos = int(variant[1])
    for mapping in scaffold_map_dict[variant[0]]:
        if (mapping.old_pos.start <= pos <= mapping.old_pos.end):
            if _is_snp(variant):
                _remap_snp(variant, mapping)
            else:
                _remap_indel(variant, mapping, sequence_dict)
            return

def _update_vcf(input_file, output_dir, scaffold_map_dict, sequence_dict):
    output_file = os.path.join(output_dir, os.path.basename(input_file))
    with open(input_file, 'r') as input_vcf, open(output_file, 'w') as output_vcf:
        vcfreader = csv.reader(input_vcf, delimiter='\t')
        vcfwriter = csv.writer(output_vcf, delimiter='\t', lineterminator='\n')

        variants = []
        for line in vcfreader:
            if line[0][:2] == '##':
                output_vcf.write(line[0]+'\n')
            elif line[0][0] == '#':
                current_time = (datetime.datetime.now()).strftime('%a %d %b %Y %X ')
                output_vcf.write('## remapped '+current_time+'\n') # TODO should make this optional
                vcfwriter.writerow(line)
            else:
                variants.append(line)

        for variant in variants:
            _remap_variant(variant, scaffold_map_dict, sequence_dict)

        # Sort by sequence id, then position
        variants.sort(key=lambda x: (x[0], int(x[1])))
        for variant in variants:
            vcfwriter.writerow(variant)
    print('Remapped '+input_file)

def update_vcf(file_from, file_to, output_dir, input_file, nproc=1, index=False, verbose=False):
    """
    """
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    # TODO make index an argument
    if verbose:
        print("Loading FASTA files...")
    sequences_from = _get_fasta_sequences(file_from, index)
    sequences_to = _get_fasta_sequences(file_to, index)

    start = timer()
    if verbose:
        print("Creating sequence mapping...")
    scaffold_map = _create_sequence_mapping(sequences_from, sequences_to, nproc)[0]
    scaffold_map_dict = _get_scaffold_map_dict(scaffold_map)
    if verbose:
        print("Sequence mapping created in "+str(timer()-start)+" seconds")

    if verbose:
        print("Updating VCF file...")
    _update_vcf(input_file, output_dir, scaffold_map_dict, sequences_to)

def _get_scaffold_map_dict(scaffold_map):
    # Create a dictionary for quicker access to scaffolds by sequence id
    scaffold_map_dict = {}
    for scaffold in scaffold_map:
        if scaffold.old_pos.seq_id in scaffold_map_dict:
            scaffold_map_dict[scaffold.old_pos.seq_id].append(scaffold)
        else:
            scaffold_map_dict[scaffold.old_pos.seq_id] = [scaffold]
    return scaffold_map_dict

def measure_map_span(scaffold_map):
    span_size = 0
    for mapping in scaffold_map:
        if (hasattr(mapping, "start")):
            span_size += 1+abs(mapping.end-mapping.start)
        else:
            span_size += 1+abs(mapping.old_pos.end-mapping.old_pos.start)
    return span_size

def update_vcfs(file_from, file_to, output_dir, input_files, nproc=1, index=False, verbose=False):
    """
    """
    if len(input_files)==1:
        # No multiprocessing overhead
        update_vcf(file_from, file_to, output_dir, input_files[0], nproc, index, verbose)
        return

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    if verbose:
        print("Loading FASTA files...")
    sequences_from = _get_fasta_sequences(file_from, index)
    sequences_to = _get_fasta_sequences(file_to, index)

    start = timer()
    if verbose:
        print("Creating sequence mapping...")
    scaffold_map = _create_sequence_mapping(sequences_from, sequences_to, nproc)
    scaffold_map_dict = _get_scaffold_map_dict(scaffold_map)
    if verbose:
        print("Sequence mapping created in "+str(timer()-start)+" seconds")

    if verbose:
        print("Updating VCF files...")
    # Partial
    #update_parial = functools.partial(_update_vcf, output_dir=output_dir,
    #    scaffold_map_dict=scaffold_map_dict, sequence_dict=sequences_to)
    #start = timer()

    # Callable wrapper class avoids Pool pickling overhead
    # TODO benchmark wrapper class against the partial above
    update_wrapper = _update_vcf_wrapper(output_dir,
                                         scaffold_map_dict,
                                         sequences_to)
    pool = multiprocessing.Pool(nproc)
    pool.map(update_wrapper, input_files)
    pool.close()
    pool.join()

class _update_vcf_wrapper:
    # Needed to avoid the pickling overhead when using an _update_vcf
    # partial with Pool - the sequence dictionary is very large.
    def __init__(self, output_dir, scaffold_map_dict, sequence_dict):
        self.output_dir = output_dir
        self.scaffold_map_dict = scaffold_map_dict
        self.sequence_dict = sequence_dict

    def __call__(self, input_file):
        _update_vcf(input_file, self.output_dir,
                    self.scaffold_map_dict, self.sequence_dict)
