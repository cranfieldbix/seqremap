#!/usr/bin/env python
import argparse
import seqremap
from timeit import default_timer as timer
import sys
import time

parser = argparse.ArgumentParser(description='Remap VCF files from one version of a reference genome to another.')
parser.add_argument('old_reference', type=str, help='old reference genome FASTA')
parser.add_argument('new_reference',  type=str, help='new reference genome FASTA')
parser.add_argument('output_directory', type=str, help='output directory')
parser.add_argument('input_files', nargs='+', type=str, help='VCF files to be updated')
parser.add_argument('--np', required=False, default=1, type=int, help='number of processes to use')
parser.add_argument('--index', required=False, action='store_true', help='use indexing to access genome files (slower, less memory use)')
parser.add_argument('--add_comment', required=False, action='store_true', help='add a comment specifying the time the update was made')
parser.add_argument('-v', required=False, action='store_true', help='verbose mode')

args = parser.parse_args()

start = timer()

seqremap.update_vcfs(args.old_reference, args.new_reference, args.output_directory, 
                     args.input_files, nproc=args.np, index=args.index, verbose=args.v)

if args.v:
    print("Total runtime: "+str(timer()-start))
