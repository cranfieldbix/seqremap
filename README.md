# Tools for mapping Variant Coordinates between genomes #


## What is this repository for? ##

This software provides a function which compares two sets of sequences and
creates a "mapping" between them, listing the respective positions of
contigs/scaffolds in both sets.

When comparing two versions of a reference genome, this mapping
represents a list of differences in scaffold position and orientation
between them.

The module also provides functions which can use a "scaffold mapping" to
update the coordinates and sequences in variant call format (VCF) files.

No extra information (e.g. AGP files) about the actual contig/scaffold
organisation of the genomes is required, as this is inferred from the
sequences.

"Contigs" are defined as stretches of sequence containing no more than
one N base in a row. "Scaffolds" are defined as groups of adjacent
contigs which have the same relative order, position, and orientation
in both sets of sequences being compared.

## HOW TO USE SeqRemap ##
./update_vcf.py old_reference [old_reference.fa(sta)] new_reference [new_reference.fa(sta)] output_directory [my_folder] input_files [vcf1, vcf2, ...]

Remap VCF files from one version of a reference genome to another.

positional arguments:
  old_reference     old reference genome FASTA
  new_reference     new reference genome FASTA
  output_directory  output directory
  input_files       VCF files to be updated

optional arguments:
  -h, --help        show this help message and exit
  --np NP           number of processes to use
  --index           use indexing to access genome files (slower, less memory
                    use)
  --add_comment     add a comment specifying the time the update was made
  -v                verbose mode

## 1. To Create a sequence position mapping between two FASTA files (e.g. two reference genome versions): ##
    create_sequence_mapping(file_from, file_to, index, group)
    
## 2. To Update multiple VCF files based on a sequence mapping: ##
    update_vcfs(output_dir, scaffold_map, *input_files)

## 3. To update a single VCF file based on a sequence mapping: ##
    update_vcf(input_file, output_file, scaffold_map)


## How do I get set up? (Linux and macOS are currently supported)##

1. Dowload SeqRemap using the link https://fmohareb@bitbucket.org/cranfieldbix/seqremap.git or clone the repository directly using the following git command:
git clone https://fmohareb@bitbucket.org/cranfieldbix/seqremap.git

2. Make sure that Python and BioPython are installed 
For more information on installing BioPython, please follow the link: http://biopython.org/DIST/docs/install/Installation.html#htoc3


### Who do I talk to? ###
* Tomasz Kurowski (Lead developer): t.kurowski@cranfield.ac.uk
* Fady Mohareb (Head of Bioinformatics Group, Cranfield University): f.mohareb@cranfield.ac.uk
